module.exports =  class ClientRepository{
    constructor(){
        
        this.connectedClients = [];
        this.highScores = [];
        this.idQueue = [1,2,3,4,5,6];
    }

    addScore(playerName, score){
        const playerNameScore = {name: playerName, score: score}
    }

    addClient(playerName, socket){

        const client = {
            id: this.idQueue.shift(),
            name: playerName,
            socket: socket
        }
        
        this.connectedClients.push(client);

        return client;
    }

    removeClientBySocket(socket){
        let removedClient = null;
        this.connectedClients = this.connectedClients.filter((client) => {
            if(socket === client.socket){
                this.idQueue.push(client.id);
                removedClient = client;
            }else{
                return client;
            }
        });

        return removedClient;
    }

    broadcastAll(message){
        this.connectedClients.forEach((client) => client.socket.writeTetrinetMessage(message))
    }

    broadcastAllExcept(message, id){
        this.connectedClients.forEach((client) => {
            if( client.id !== id ){
                client.socket.writeTetrinetMessage(message)
            }
        });
    }

    hasEmptySlot(){
        return this.idQueue.length > 0;
    }




}