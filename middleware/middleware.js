//@ts-check

const net = require('net');
const decodeLogin = require('./protocol/decodeLogin');
const tetrinetProtocolIO = require('./protocol/IO');
const isLoginMessage = require('./protocol/isLoginMessage');

var defaultWrite = net.Socket.prototype.write;
//net.Socket.prototype.write = function(data, cb){
//    defaultWrite(tetrinetProtocolIO.encode(data), cb)
//} 

class TetrijsSocket{
    /**
     * 
     * @param {any} socket 
     */
    constructor(socket){
        this.socket = socket;
    }

    /**
     * 
     * @param {string} message 
     */
    writeTetrinetMessage(message){
        this.socket.write(tetrinetProtocolIO.encode(message));
    }
}




const App = () => {

    let commandsCallbacksObj = [];
    /**
     * 
     * @param {(socket: TetrijsSocket) => void} callback 
     */
    let onSocketConnectCallback;
    let onSocketDisconnectCallback;

    /**
     * 
     * @param {String} command 
     * @param {(params: String, socket: TetrijsSocket)} callback 
     */
    const _on = (command, callback) => {
        commandsCallbacksObj.push({command: command, callback: callback});
    }


    const server = net.createServer((socket) => {
        let tSocket = new TetrijsSocket(socket);
       
        let connectionApproved = true;
        if(onSocketConnectCallback){
            connectionApproved = onSocketConnectCallback(tSocket);
        }

        if(!connectionApproved){
            return;
        }

        tSocket.socket.on('data', (data) => {
            
            let message = tetrinetProtocolIO.read(data);
            
            if(isLoginMessage(message)){
                message = decodeLogin( message );
            }
     
            let splitedMessage = message.split(' ');
            const command = splitedMessage[0];
            const params = splitedMessage.slice(1).join(' ');
            let commandCallbackObj = commandsCallbacksObj.find(x => x.command === command);
            if(commandCallbackObj){
                commandCallbackObj.callback(params, tSocket);
            }
            

        });

        //end

        tSocket.socket.on('end', () => {
            onSocketDisconnectCallback(tSocket);
        })
    });
   
    return {
        onReceive: _on,
        server: server,
        
        /**
         * 
         * @param {(socket: TetrijsSocket) => boolean} callback 
         */
        setOnConnectionListener: function(callback){
            onSocketConnectCallback = callback;
        },
        /**
         * 
         * @param {(socket: TetrijsSocket) => void} callback 
         */
        setOnSocketDisconnectCallback: function(callback){
            onSocketDisconnectCallback = callback;
        }
    }
}

module.exports = App;