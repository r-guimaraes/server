const _END_LINE_HEX_CHAR = 'FF'; 

const _stringToHex = (string) => {
    return new Buffer(string).toString('hex');
}

const _stringToProtocolHex = (string) => {
    return _stringToHex(string) + _END_LINE_HEX_CHAR;
}

/**
 * @description Formata a messagem definida no protocolo tetrinet
 * para o formato de envio para o cliente.
 * @param {String} message 
 */
const _encode = (message) => {
    strProtocolHex = _stringToProtocolHex(message);
    return Buffer.from(strProtocolHex, 'hex');
}

/**
 * @description converte o buffer recebido do cliente para string removendo o caractere FF definido
 * no protocolo tetrinet
 * @param {*} data 
 */
const _read = (data) => {
    return data.toString().slice(0, data.toString().length - 1);
}




module.exports = {
    /**
     * 
     * @param {*} data 
     */
    read: _read,

    encode: _encode
}