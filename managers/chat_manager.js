//@ts-check
const ClientRepo = require('../client_repository/ClientRepository');

/**
 * 
 * @param {*} app 
 * @param {ClientRepo} clientRepository 
 */
const chatManager = function(app, clientRepository){


	app.onReceive('pline', (params) => {
		params = String(params);
		const seederId = parseInt( params.split(' ')[0] );
		const message = params.split(' ').slice(1).join(' ');

		//broadcast message to other clients
		const msg = `pline ${seederId} ${message}`;
		clientRepository.broadcastAllExcept(msg, seederId);
	});
	
	app.onReceive('plineact', (params) => {
		params = String(params);
		//params = '<playernumber> <action>'
	});

	app.onReceive('gmsg', (params) => {
		params = String(params);
		clientRepository.broadcastAll(params);
		//params = '<message>'
	})

}

module.exports = chatManager;